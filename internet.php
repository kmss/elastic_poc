<?php
include 'header.php';
?>
      <div id="content-wrapper">

        <div class="container-fluid">

		  <h2>Internet</h2>
		  
			<br>

          <!-- Icon Cards-->
          <div class="row">
            <div class="col-xl-4 col-sm-6 mb-4">
			
              <div class="rogers-card">
		
				<h3>Plans, Offers & Add-Ons</h3>
				<a href='#' class='card-link'>Offers <i class="fas fa-angle-right"></i></a><br>				
        <a href='#' class='card-link'>Packages & Add-ons <i class="fas fa-angle-right"></i></a><br>				
        <br><br>		
              </div>
            </div>

              <div class="col-xl-4 col-sm-6 mb-4">
			
      <div class="rogers-card">

<h3>Equipment</h3>
<a href='#' class='card-link'>Modems & Other <i class="fas fa-angle-right"></i></a><br>				
<a href='#' class='card-link'>Return & Exchanges <i class="fas fa-angle-right"></i></a><br>				
<a href='#' class='card-link'>Self-Installation, Modem & Wi-Fi Setup <i class="fas fa-angle-right"></i></a><br>				
<a href='#' class='card-link'>Troubleshoot <i class="fas fa-angle-right"></i></a><br>						
      </div>
    </div>


  <div class="col-xl-4 col-sm-6 mb-4">
			
      <div class="rogers-card">

<h3>Activations, Installations & Setup</h3>
<a href='#' class='card-link'>Activate an Account <i class="fas fa-angle-right"></i></a><br>				
<a href='#' class='card-link'>Installations & Technician Appointments <i class="fas fa-angle-right"></i></a><br>				
<a href='#' class='card-link'>Self-Installation, Modem & Wi-Fi Setup <i class="fas fa-angle-right"></i></a><br>		
<br>				
      </div>
    </div>
        </div>
		
		
		<div class='row'>
		
		  <div class="col-xl-4 col-sm-6 mb-4">
				<div class="rogers-card">
				<h3>Billing & Payments</h3>
        
          <a href='#' class='card-link'>Billing & Online Billing <i class="fas fa-angle-right"></i></a><br>				
          <a href='#' class='card-link'>Cancelled & Suspended Accounts <i class="fas fa-angle-right"></i></a><br>				
          <a href='#' class='card-link'>Charges & Credits <i class="fas fa-angle-right"></i></a><br>				
          <a href='#' class='card-link'>Payments, Refunds & Arrangements <i class="fas fa-angle-right"></i></a><br>				
              </div>
            </div>
       
		  
		  
		    <div class="col-xl-4 col-sm-6 mb-4">
				<div class="rogers-card">
				<h3>Manage Accounts</h3>
				<a href='#' class='card-link'>Cancel, Move or Suspend Service <i class="fas fa-angle-right"></i></a><br>				
				<a href='#' class='card-link'>Change Account Info or Ownership <i class="fas fa-angle-right"></i></a><br>				
				<a href='#' class='card-link'>Change Package <i class="fas fa-angle-right"></i></a><br>				
				<a href='#' class='card-link'>Manage Usage <i class="fas fa-angle-right"></i></a><br>			
              </div>
            </div>

		   <div class="col-xl-4 col-sm-6 mb-4">
				<div class="rogers-card">
				<h3>Network & Troubleshooting</h3>
				<a href='#' class='card-link'>Appointments, Tickets & Outages <i class="fas fa-angle-right"></i></a><br>				
				<a href='#' class='card-link'>Troubleshoot Connectivity <i class="fas fa-angle-right"></i></a><br>				
				<a href='#' class='card-link'>Troubleshoot Email <i class="fas fa-angle-right"></i></a><br>				
				<a href='#' class='card-link'>Troubleshoot Equipment <i class="fas fa-angle-right"></i></a><br>		
              </div>
            </div>

    		</div>
        <br>

      <nav>
      <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">My Internet Bookmarks</a>
        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">My Popular Internet Articles</a>
      </div>
    </nav>
    <div class="tab-content" id="nav-tabContent">
      <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
        <a href='#' class='card-link'>Authenticate a Monthly Subscriber Contact Through ICM <i class="fas fa-angle-right"></i></a><br>
        <a href='#' class='card-link'>Request US Credit Check <i class="fas fa-angle-right"></i></a><br>
        <a href='#' class='card-link'>Dialling an International Long Distance Call From Canada <i class="fas fa-angle-right"></i></a><br>

      </div>
      <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
      <a href='#' class='card-link'>Moves Concierge <i class="fas fa-angle-right"></i></a><br>
        <a href='#' class='card-link'>Cable Offers - TV, Internet and RHP Offers <i class="fas fa-angle-right"></i></a><br>       
        

      </div>

    </div>

		</div></div>
        <!-- /.container-fluid -->

      <?php
include 'footer.php';
?>