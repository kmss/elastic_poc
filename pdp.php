<?php
include 'header.php';
?>

<div id="content-wrapper">

    <div class="container-fluid">

    	<h2>Premium Device Protection</h2>
    	<br>

		<div class="row">
            <div class="col-xl-3 col-sm-6 mb-6">

			<div class='rogers-card'>
				<h3>Support</h3>
				Brightstar Customers contact Brightstar directly to use Premium Device Protection
				<br><br>
				<i class="fas fa-phone"></i> : 1-800-345-2456 <br>
				<i class="fas fa-at"></i> : support@brightstar.com <br>


				<div class="collapse" id="offers">
					<hr>
					<span style="font-family: TedNext-Bold">Brightstar Tier 2</span><br>
					
					Use of the below information is limited to escalation groups only.
					Distribution of this information is not permitted by any teams.<br><br>
					<i class="fas fa-phone"></i> : 1-855-775-0490<br>
					<i class="fas fa-at"></i> : RogersFidoSupport@brightstar.com

	
					</div>

					<br>
					<div align='right'>
						<button class='btn-info btn-xs' id="button" type="button" data-toggle="collapse" data-target="#offers" aria-expanded="false" aria-controls="collapseExample">Show More <i class="far fa-caret-square-down"></i></button>
					</div>


			</div>




			<br>

			<div class='rogers-card'>
			<h3>Features</h3>

			<ul>
				<li>All provincial eligibilities for Premium Device Protection follow the billing address.</li>
				<li>First screen break repair for no extra fee, as long as there are no other damages to the device.</li>
				<li>Device Replacement for loss and theft (Not available in QC - Brightstar will not make exceptions).</li>
				<li>Device repair or replacement for damage outside of manufacturer’s warranty e.g. defects, physical or liquid damage.</li>
				<li>Access to convenient repair options including, mail-in, walk-in and mobile technician visits.</li>
				<li>Speedy replacements as early as next day and in some instances same day also available for an additional fee.</li>
				<li>12 month warranty on replacement devices.</li>
				<li>Coverage for any device the enroled CTN’s SIM card is used in.</li>
			</ul>
			</div>


			</div>
            
		<div class="col-xl-9 col-sm-6 mb-6">

			<div class='rogers-card'>
				<h3> Pricing</h3>

				Please make a selection: &nbsp;
				<select class="selectpicker" title="Province">
				  <option value="Alberta">Alberta</option>
				  <option value="British Columbia">British Columbia</option>
				  <option value="Manitoba">Manitoba</option>
				  <option value="National">National</option>
				  <option value="Newfoundland">Newfoundland</option>
				  <option value="New Brunswick">New Brunswick</option>
				  <option value="Northwest Territories">Northwest Territories</option>
				  <option value="Nova Scotia">Nova Scotia</option>
				  <option value="Nunavut">Nunavut</option>
				  <option value="Ontario">Ontario</option>
				</select>
				
				<select class="selectpicker" title="Customer Type" id='custType'>
				  <option value="CR">Consumer-Regular</option>
				  <option value="BR">Business-Regular</option>
				</select>

				

				<div id='pricingResults' style="display:none; margin:auto; width: 80%;" class='col-sm-8'>
					<br>
					<table class='table table-bordered table-sm' style='text-align:center;'>
						<tr  class='table-info' style='font-family: TedNext-Bold;'>
						
							<td >Code</td>
							<td>Price</td>
							<td>Start</td>
							<td>End</td>

						
						</tr>

						<tr>
							<td>DPROC000F</td>
							<td>$11/mo</td>
							<td>01-Feb-2019</td>
							<td>Ongoing</td>
						</tr>
					</table>

				</div>

			</div>

			<br>

			<div class='rogers-card'>

				<hgroup class="mb20">
				<h3>Search Results</h3>
				<h2 class="lead"><strong class="text-danger">12</strong> results were found for <strong class="text-danger">Premium Device Protection</strong></h2>								
				</hgroup>

			<div class="card">
				<div class="card-body">
			
				Article Refinements:  &nbsp;

				<select class="selectpicker" multiple title="Life Cycle">
				  <option value="Learn">Learn</option>
				  <option value="Buy">Buy</option>
				  <option value="Get">Get</option>
				  <option value="Use">Use</option>
				  <option value="Pay">Pay</option>
				  <option value="Support">Support</option>
				</select>
				
				<select class="selectpicker" multiple title="Document Type">
				  <option value="FAQs">FAQs</option>
				  <option value="Offers">Offers</option>
				  <option value="Plans and Packages">Plans and Packages</option>
				</select>
				
				<select class="selectpicker" multiple title="Province">
				  <option value="Alberta">Alberta</option>
				  <option value="British Columbia">British Columbia</option>
				  <option value="Manitoba">Manitoba</option>
				  <option value="National">National</option>
				  <option value="Newfoundland">Newfoundland</option>
				  <option value="New Brunswick">New Brunswick</option>
				  <option value="Northwest Territories">Northwest Territories</option>
				  <option value="Nova Scotia">Nova Scotia</option>
				  <option value="Nunavut">Nunavut</option>
				  <option value="Ontario">Ontario</option>
				</select>

				<button class="btn btn-info" data-bind="click: findClick">Filter</button>
			</div></div>

			<br>
				<section class="col-xs-12 col-sm-6 col-md-12">

					<article class="search-result row">
		
						<div class="col-xs-12 col-sm-12 col-md-8">
							<h4><a class='results-header' href="#" title="">Voluptatem, exercitationem, suscipit, distinctio</a></h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem, exercitationem, suscipit, distinctio, qui sapiente aspernatur molestiae non corporis magni sit sequi iusto debitis delectus doloremque.</p>						
						</div>
						<div class="col-xs-12 col-sm-12 col-md-4">
							<ul class="meta-search">
								<li><i class="far fa-calendar-alt"></i> <span>02/13/2014</span></li>
								<li><i class="far fa-clock"></i> <span>8:32 pm</span></li>
								<li><i class="fas fa-tags"></i></i> <span>People</span></li>
							</ul>
						</div>
					</article>


				


						<article class="search-result row">
		
						<div class="col-xs-12 col-sm-12 col-md-8">
							<h4><a class='results-header' href="#" title="">Voluptatem, exercitationem, suscipit, distinctio</a></h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem, exercitationem, suscipit, distinctio, qui sapiente aspernatur molestiae non corporis magni sit sequi iusto debitis delectus doloremque.</p>						
						</div>
						<div class="col-xs-12 col-sm-12 col-md-4">
							<ul class="meta-search">
								<li><i class="far fa-calendar-alt"></i> <span>02/13/2014</span></li>
								<li><i class="far fa-clock"></i> <span>8:32 pm</span></li>
								<li><i class="fas fa-tags"></i></i> <span>Food, People</span></li>
							</ul>
						</div>
					</article>

				<div class="panel-group" id="accordion">
					  <div class="card card-default">
					    <div class="card-header" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne">
					        <a class="collapse-header">
					          FAQ #1
					        </a>
									<div style="float: right;" class='toggleChevron'><i class="fas fa-chevron-down"></i></div>
					        
					    </div>
					    <div id="collapseOne" class="collapse faq-collapse">
					      <div class="faq-body">
					        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
					      </div>
					    </div>
					  </div>

					  <div class="panel-group" id="accordion">
					  <div class="card card-default">
					    <div class="card-header" data-toggle="collapse" data-parent="#accordion" data-target="#collapseTwo">
					        <a class="collapse-header">
					          FAQ #2
					        </a>
									<div style="float: right;" class='toggleChevron'><i class="fas fa-chevron-down"></i></div>

					    </div>
					    <div id="collapseTwo" class="collapse faq-collapse">
					      <div class="faq-body">
					        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem, exercitationem, suscipit, distinctio, qui sapiente aspernatur molestiae non corporis magni sit sequi iusto debitis delectus doloremque.
					      </div>
						</div>
					</div>
					  

					  <div class="card card-default">
					    <div class="card-header" data-toggle="collapse" data-parent="#accordion" data-target="#collapseThree">

					        <a class="collapse-header">
					          FAQ #3
							</a>
		

							<div style="float: right;" class='toggleChevron'><i class="fas fa-chevron-down"></i></div>
							
					    </div>
					    <div id="collapseThree" class="collapse faq-collapse">
					      <div class="faq-body">
					        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
					      </div>
						</div>
					</div>

						<br>

						<article class="search-result row">
		
						<div class="col-xs-12 col-sm-12 col-md-8">
							<h4><a class='results-header' href="#" title="">Voluptatem, exercitationem, suscipit, distinctio</a></h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem, exercitationem, suscipit, distinctio, qui sapiente aspernatur molestiae non corporis magni sit sequi iusto debitis delectus doloremque.</p>						
						</div>
						<div class="col-xs-12 col-sm-12 col-md-4">
							<ul class="meta-search">
								<li><i class="far fa-calendar-alt"></i> <span>02/13/2014</span></li>
								<li><i class="far fa-clock"></i> <span>8:32 pm</span></li>
								<li><i class="fas fa-tags"></i></i> <span>Travel</span></li>
							</ul>
						</div>
					</article>

						<article class="search-result row">
		
					<div class="col-xs-12 col-sm-12 col-md-8">
						<h4><a class='results-header' href="#" title="">Voluptatem, exercitationem, suscipit, distinctio</a></h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem, exercitationem, suscipit, distinctio, qui sapiente aspernatur molestiae non corporis magni sit sequi iusto debitis delectus doloremque.</p>						
					</div>
					<div class="col-xs-12 col-sm-12 col-md-4">
						<ul class="meta-search">
							<li><i class="far fa-calendar-alt"></i> <span>02/13/2014</span></li>
							<li><i class="far fa-clock"></i> <span>8:32 pm</span></li>
							<li><i class="fas fa-tags"></i></i> <span>Food</span></li>
						</ul>
					</div>
				</article>

					  

				</div>
		</section>
</div>

</div>

<?php
include 'footer.php';
?>