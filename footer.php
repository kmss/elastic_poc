  <!-- Sticky Footer -->
        <footer class="sticky-footer">

            <table style='width:100%; height:100%;'>

              <tr>
                <td class='footer-td'> &nbsp;&nbsp;<b>More tools &nbsp;<i class="fas fa-chevron-down"></i> </b> </td>
                <td class='footer-td footer-center'> Tax Calculator </td>
                <td class='footer-td footer-center'> Roaming Tool </td>
                <td class='footer-td footer-center'> Compare TV Packages </td>
                <td colspan = '2' class='footer-td' align='right'>  Help & Feedback <i class="fas fa-chevron-up"></i> &nbsp;&nbsp; &nbsp;
				<img src='img/yammer-logo.png'> &nbsp;&nbsp;&nbsp;&nbsp;
				<img src='img/bell.png' width='25px'> &nbsp;&nbsp;
				</td>
              </tr>
            </table>

            <!--
            <div class="copyright text-center my-auto">
              <span>Copyright © Your Website 2018</span>
            </div>
          -->
        </footer>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>


    <!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> -->
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>


    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Page level plugin JavaScript-->
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>


    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.js"></script>
    <script src="js/rogers.js"></script>

    <!-- Demo scripts for this page
    <script src="js/demo/datatables-demo.js"></script>
    <script src="js/demo/chart-area-demo.js"></script>
    -->

  </body>

</html>
