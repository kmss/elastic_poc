<?php
include 'header.php';
?>


<!--
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
-->

<div id="content-wrapper">
	<div class="container-fluid">
		<div style="padding: 0px 15px 0px 15px">
			<hgroup class="mb20">
				<h1>Search Results</h1>
				<h2 class="lead"><strong class="text-danger">3</strong> results were found for <strong class="text-danger">Lorem</strong></h2>								
			</hgroup>

			<div class="card">
				<div class="card-body">
			
				<select class="selectpicker" multiple title="Location">
				  <option value="Alberta">Alberta</option>
				  <option value="British Columbia">British Columbia</option>
				  <option value="Manitoba">Manitoba</option>
				  <option value="National">National</option>
				  <option value="Newfoundland">Newfoundland</option>
				  <option value="New Brunswick">New Brunswick</option>
				  <option value="Northwest Territories">Northwest Territories</option>
				  <option value="Nova Scotia">Nova Scotia</option>
				  <option value="Nunavut">Nunavut</option>
				  <option value="Ontario">Ontario</option>
				</select>
				
				<select class="selectpicker" multiple title="Document Type">
				  <option value="FAQs">FAQs</option>
				  <option value="Offers">Offers</option>
				  <option value="Plans and Packages">Plans and Packages</option>
				</select>
				
				<select class="selectpicker" title="Line of Business">
				  <optgroup label="Rogers">
					<option>Mustard</option>
					<option>Ketchup</option>
					<option>Relish</option>
				  </optgroup>
				  <optgroup label="Fido">
					<option>Tent</option>
					<option>Flashlight</option>
					<option>Toilet Paper</option>
				  </optgroup>
				</select>
				<!--<div class="row"> 
					<div class="col-sm-offset-2 col-sm-10">-->
					  <button class="btn btn-primary" data-bind="click: findClick">Filter</button>
				<!--  </div>  
				</div>  --->
				
				</div>
			</div>
			<br />
			<section class="col-xs-12 col-sm-6 col-md-12">
			<article class="search-result row">
				<!--<div class="col-xs-12 col-sm-12 col-md-3">
					<a href="#" title="Lorem ipsum" class="thumbnail"><img src="https://images.ctfassets.net/8utyj17y1gom/1aNxrjyFdOSYMYY6cYIYa2/40df355dac5fc0dbd26ac186d70d40a8/SHM-Control-Full.png" alt="Lorem ipsum" width='250px'/></a>
				</div> -->

				<div class="col-xs-12 col-sm-12 col-md-7 excerpet">
					<h3><a href="#" title="" class='results-header'>Voluptatem, exercitationem, suscipit, distinctio</a></h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem, exercitationem, suscipit, distinctio, qui sapiente aspernatur molestiae non corporis magni sit sequi iusto debitis delectus doloremque.</p>						
					<!-- <span class="plus"><a href="#" title="Lorem ipsum"><i class="fas fa-plus-square"></i></a></span> -->
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4">
					<ul class="meta-search">
						<li><i class="far fa-calendar-alt"></i> <span>02/15/2018</span></li>
						<li><i class="far fa-clock"></i> <span>4:28 pm</span></li>
						<li><i class="fas fa-tags"></i></i> <span>People</span></li>
					</ul>
				</div>
				<!-- <span class="clearfix borda"></span> -->
			</article>

			<article class="search-result row">
				<!--<div class="col-xs-12 col-sm-12 col-md-3">
					<a href="#" title="Lorem ipsum" class="thumbnail"><img src="https://images.ctfassets.net/8utyj17y1gom/XGQVNYcocUcsmImUwyWuO/53961d71ee847aca6d63fd9b89ca47d9/bundle-select.png" alt="Lorem ipsum" width='250px'/></a>
				</div> --->
			
				<div class="col-xs-12 col-sm-12 col-md-7">
					<h3><a class='results-header' href="#" title="">Voluptatem, exercitationem, suscipit, distinctio</a></h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem, exercitationem, suscipit, distinctio, qui sapiente aspernatur molestiae non corporis magni sit sequi iusto debitis delectus doloremque.</p>						
					<!--<span class="plus"><a href="#" title="Lorem ipsum"><i class="fas fa-plus-square"></i></a></span> -->
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4">
					<ul class="meta-search">
						<li><i class="far fa-calendar-alt"></i> <span>02/13/2014</span></li>
						<li><i class="far fa-clock"></i> <span>8:32 pm</span></li>
						<li><i class="fas fa-tags"></i></i> <span>Food</span></li>
					</ul>
				</div>
			</article>

			<article class="search-result row">
				<!-- <div class="col-xs-12 col-sm-12 col-md-3">
					<a href="#" title="Lorem ipsum" class="thumbnail"><img src="https://images.ctfassets.net/8utyj17y1gom/4kY9dOLQIMuAoUMSOS4G64/61e625528b083b94d7892f78a79c1809/Moves-Full.jpg" alt="Lorem ipsum" width='250px'/></a>
				</div> 
				<div class="col-xs-12 col-sm-12 col-md-2">
					<ul class="meta-search">
						<li><i class="far fa-calendar-alt"></i> <span>01/11/2014</span></li>
						<li><i class="far fa-clock"></i> <span>10:13 am</span></li>
						<li><i class="fas fa-tags"></i> <span>Sport</span></li>
					</ul>
				</div>--->
				<div class="col-xs-12 col-sm-12 col-md-7">
					<h3><a class='results-header' href="#" title="">Voluptatem, exercitationem, suscipit, distinctio</a></h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem, exercitationem, suscipit, distinctio, qui sapiente aspernatur molestiae non corporis magni sit sequi iusto debitis delectus doloremque.</p>						
					
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4">
					<ul class="meta-search">
						<li><i class="far fa-calendar-alt"></i> <span>01/11/2014</span></li>
						<li><i class="far fa-clock"></i> <span>10:13 am</span></li>
						<li><i class="fas fa-tags"></i> <span>Sport</span></li>
					</ul>
				</div>
				<!--- <span class="clearfix border"></span> --->
			</article>			
			</section>

		</div>
	</div>
</div>

<?php
include 'footer.php';
?>