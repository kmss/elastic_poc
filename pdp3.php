<?php
include 'header.php';
?>


<div id="content-wrapper">

    <div class="container-fluid">

    	<h2>Premium Device Protection</h2>
    	<br>

		<div class="row">
            <div class="col-xl-2 col-sm-6 mb-6">

			<div class='rogers-card'>
			<h3>Contact</h3>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ut leo at ex feugiat imperdiet quis sit amet ante. Fusce eu lectus et arcu finibus ultricies eu quis lorem.			</div>

			<br>

			<div class='rogers-card'>
			<h3>Support</h3>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ut leo at ex feugiat imperdiet quis sit amet ante. Fusce eu lectus et arcu finibus ultricies eu quis lorem. Suspendisse ut ornare turpis.			</div>


			</div>
            
		<div class="col-xl-10 col-sm-6 mb-6">

    	<nav>
		  <div class="nav nav-tabs" id="nav-tab" role="tablist">
		    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Home</a>
		    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-learn" role="tab" aria-controls="nav-profile" aria-selected="false">Learn</a>
				<a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-buy" role="tab" aria-controls="nav-contact" aria-selected="false">Buy</a>
				<a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-get" role="tab" aria-controls="nav-contact" aria-selected="false">Get</a>

		    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-use" role="tab" aria-controls="nav-contact" aria-selected="false">Use</a>

		    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-pay" role="tab" aria-controls="nav-contact" aria-selected="false">Pay</a>

		    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-support" role="tab" aria-controls="nav-contact" aria-selected="false">Support</a>

		  </div>
		</nav>
		<div class="tab-content" id="nav-tabContent">
		  <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
				<img src='img/image.png' width='80%'>
				<br><br>
				<p>
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
				Integer ut leo at ex feugiat imperdiet quis sit amet ante. 
				Fusce eu lectus et arcu finibus ultricies eu quis lorem. 
				Suspendisse ut ornare turpis. Aenean tristique dolor tellus, in consequat dui luctus vitae. 
				Nunc convallis cursus cursus. Integer dignissim purus quis neque maximus porta. 
				Quisque ac neque purus. Fusce ut sapien quis odio facilisis condimentum.
			</p>
		  </div>
		  <div class="tab-pane fade" id="nav-learn" role="tabpanel" aria-labelledby="nav-profile-tab">
				<h3>Learn </h3>
				
				<a href='#' class='card-link' data-toggle="collapse" data-target="#demo" >Question 1 - How do I get PDP for my phone?  <i class="fas fa-angle-right"></i></a><br>				

				<div id="demo" class="collapse">
					<div class='card answer-card'>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit,
					sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
					</div>
				</div>



				<a href='#' class='card-link' data-toggle="collapse" data-target="#demo2" >Question 2 - How much does it cost?   <i class="fas fa-angle-right"></i></a><br>				

				<div id="demo2" class="collapse">
					<div class='card answer-card'>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit,
					sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
					</div>
				</div>


				<a href='#' class='card-link' data-toggle="collapse" data-target="#demo3" >Question 3 - Where do I sign up?   <i class="fas fa-angle-right"></i></a><br>				

				<div id="demo3" class="collapse">
					<div class='card answer-card'>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit,
					sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
					</div>
				</div>


				<a href='#' class='card-link' data-toggle="collapse" data-target="#demo4" >Question 4 - Is it worth it?   <i class="fas fa-angle-right"></i></a><br>				

			<div id="demo4" class="collapse">
				<div class='card answer-card'>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit,
				sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
				</div>
			</div>




	

		  </div>
		  <div class="tab-pane fade" id="nav-buy" role="tabpanel" aria-labelledby="nav-contact-tab">
		  	buy
			</div>
			
			<div class="tab-pane fade" id="nav-get" role="tabpanel" aria-labelledby="nav-contact-tab">
		  	get
			</div>
			
			<div class="tab-pane fade" id="nav-use" role="tabpanel" aria-labelledby="nav-contact-tab">
		  	use
			</div>
			
			<div class="tab-pane fade" id="nav-pay" role="tabpanel" aria-labelledby="nav-contact-tab">
		  	pay
			</div>
			
			<div class="tab-pane fade" id="nav-support" role="tabpanel" aria-labelledby="nav-contact-tab">
		  	support
		  </div>
		</div>

		</div>
		</div>

    </div>
</div>



<?php
include 'footer.php';
?>