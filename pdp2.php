<?php
include 'header.php';
?>

<div id="content-wrapper">

    <div class="container-fluid">

    	<h2>Premium Device Protection</h2>
    	<br>

		<div class="row">
            <div class="col-xl-3 col-sm-6 mb-6">

			<div class='rogers-card'>
				<h3>Contact</h3>
				<i class="fas fa-phone"></i> : 1-800-345-2456 <br>
				<i class="fas fa-at"></i> : support@rogerspdp.ca <br>
				<i class="fas fa-envelope"></i> : 23 Fake St, Toronto ON, M3J 4S1
			</div>

			<br>

			<div class='rogers-card'>
			<h3>Support</h3>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ut leo at ex feugiat imperdiet quis sit amet ante. Fusce eu lectus et arcu finibus ultricies eu quis lorem. Suspendisse ut ornare turpis.		

			<br><br>	
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ut leo at ex feugiat imperdiet quis sit amet ante. Fusce eu lectus et arcu finibus ultricies eu quis lorem. Suspendisse ut ornare turpis.	

			</div>


			</div>
            
		<div class="col-xl-9 col-sm-6 mb-6">

			<div class='rogers-card'>
				<h3> Pricing</h3>

					<a href='#' class='card-link'>Pricing 1 <i class="fas fa-angle-right"></i></a><br>									
					<a href='#' class='card-link'>Pricing 2 <i class="fas fa-angle-right"></i></a><br>				

					<div class="collapse" id="offers">
						<a href='#' class='card-link'>Pricing 3 <i class="fas fa-angle-right"></i></a><br>									
						<a href='#' class='card-link'>Pricing 4 <i class="fas fa-angle-right"></i></a><br>			
						<a href='#' class='card-link'>Pricing 5 <i class="fas fa-angle-right"></i></a><br>									
						<a href='#' class='card-link'>Pricing 6 <i class="fas fa-angle-right"></i></a><br>			
						<a href='#' class='card-link'>Pricing 7 <i class="fas fa-angle-right"></i></a><br>									
						<a href='#' class='card-link'>Pricing 8 <i class="fas fa-angle-right"></i></a><br>			

					
					
					</div>

					<div align='right'>
						<button class='btn-info btn-sm' id="button" type="button" data-toggle="collapse" data-target="#offers" aria-expanded="false" aria-controls="collapseExample">Show More <i class="far fa-caret-square-down"></i></button>
					</div>

			</div>

			<br>

			<div class='rogers-card'>

				<hgroup class="mb20">
				<h3>Search Results</h3>
				<h2 class="lead"><strong class="text-danger">3</strong> results were found for the search for <strong class="text-danger">Lorem</strong></h2>								
			</hgroup>

			<div class="card">
				<div class="card-body">
			
				<select class="selectpicker" multiple title="Location">
				  <option value="Alberta">Alberta</option>
				  <option value="British Columbia">British Columbia</option>
				  <option value="Manitoba">Manitoba</option>
				  <option value="National">National</option>
				  <option value="Newfoundland">Newfoundland</option>
				  <option value="New Brunswick">New Brunswick</option>
				  <option value="Northwest Territories">Northwest Territories</option>
				  <option value="Nova Scotia">Nova Scotia</option>
				  <option value="Nunavut">Nunavut</option>
				  <option value="Ontario">Ontario</option>
				</select>
				
				<select class="selectpicker" multiple title="Document Type">
				  <option value="FAQs">FAQs</option>
				  <option value="Offers">Offers</option>
				  <option value="Plans and Packages">Plans and Packages</option>
				</select>
				
				<select class="selectpicker" title="Line of Business">
				  <optgroup label="Rogers">
					<option>Mustard</option>
					<option>Ketchup</option>
					<option>Relish</option>
				  </optgroup>
				  <optgroup label="Fido">
					<option>Tent</option>
					<option>Flashlight</option>
					<option>Toilet Paper</option>
				  </optgroup>
				</select>
				<button class="btn btn-info" data-bind="click: findClick">Filter</button>
			</div></div>

			<br>
				<section class="col-xs-12 col-sm-6 col-md-12">


			<article class="search-result row">
			
				<div class="col-xs-12 col-sm-12 col-md-7">
					<h4><a class='results-header' href="#" title="" data-toggle="collapse" data-target="#expand1">Voluptatem, exercitationem, suscipit, distinctio</a></h4>
							
					<div id = 'expand1' class='collapse'>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem, exercitationem, suscipit, distinctio, qui sapiente aspernatur molestiae non corporis magni sit sequi iusto debitis delectus doloremque.<br>

					</div>	
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4">
					<ul class="meta-search">
						<li><i class="far fa-calendar-alt"></i> <span>02/13/2014</span></li>
						<li><i class="fas fa-tags"></i></i> <span>Food</span></li>
					</ul>
				</div>
			</article>


			<article class="search-result row">
			
				<div class="col-xs-12 col-sm-12 col-md-7">
					<h4><a class='results-header' href="#" title="" data-toggle="collapse" data-target="#expand2">Voluptatem, exercitationem, suscipit, distinctio</a></h4>
							
					<div id = 'expand2' class='collapse'>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem, exercitationem, suscipit, distinctio, qui sapiente aspernatur molestiae non corporis magni sit sequi iusto debitis delectus doloremque.<br>

					</div>	
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4">
					<ul class="meta-search">
						<li><i class="far fa-calendar-alt"></i> <span>02/13/2014</span></li>
						<li><i class="fas fa-tags"></i></i> <span>People</span></li>
					</ul>
				</div>
			</article>


			<article class="search-result row">
			
				<div class="col-xs-12 col-sm-12 col-md-7">
					<h4><a class='results-header' href="#" title="" data-toggle="collapse" data-target="#expand3">Voluptatem, exercitationem, suscipit, distinctio</a></h4>
							
					<div id = 'expand3' class='collapse'>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem, exercitationem, suscipit, distinctio, qui sapiente aspernatur molestiae non corporis magni sit sequi iusto debitis delectus doloremque.<br>

					</div>	
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4">
					<ul class="meta-search">
						<li><i class="far fa-calendar-alt"></i> <span>02/13/2014</span></li>
						<li><i class="fas fa-tags"></i></i> <span>Food, People</span></li>
					</ul>
				</div>
			</article>


			</section>
			</div>

			

		</div>
		</div>



    </div>
</div>



<?php
include 'footer.php';
?>