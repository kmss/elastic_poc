<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="img/favicon.rogers.ico" type="image/x-icon">

    <title>Nova</title>

    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />


    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">
    <link href="css/sb-admin.min.css" rel="stylesheet">
	<link href="css/rogers-nova.css" rel="stylesheet">
  </head>

  <body id="page-top">
	<img src="img/corner.png" >
  <a href="index.php">
    <img src='img/Nova_Rogers.png' width='130px'></a> 
    <!-- <img src='img/Nova_3d.jpg' width='130px'></a> -->


    <span style='float: right'>
      <!-- <img src="img/head.png"  width='700x'> -->
      
     <img src="img/head4.png"  width='345px'>
     <!-- Welcome, Ted Rogers <i class="fas fa-chevron-down"></i> -->

     <span style=' display: inline-block; padding: 12px 20px 0px 10px; font-weight: 600'>Welcome, Firstel Lastly <i class="fas fa-chevron-down"></i></span>

    </span>

  
    
    <nav class="navbar navbar-expand navbar-light static-top" style="padding-bottom:20px; border-bottom: 0.1px solid Gainsboro;">
		
      <!-- Navbar Search -->

		<div class="col-xl-6 col-sm-6 mb-6" style="padding-left:70px">
      <form action='results.php'>
		 <div class="input-group">
          <input type="text" class="form-control" placeholder="Search for specific terms, questions, Document IDs, etc." aria-label="Search" aria-describedby="basic-addon2">
          <div class="input-group-append">
            <button class="btn btn-info">
              <i class="fas fa-search"></i>
            </button>
          </div>
        </div>
      </form>
      </div>

      <div class="col-xl-6 col-sm-6 mb-6" align="right">
        <i class="fas fa-bookmark fa-lg text-danger"></i>
      </div>


    </nav>

    <div id="wrapper">

      <!-- Sidebar -->
      <ul class="sidebar navbar-nav">
	  
	  <li class="nav-item">
          <a class="nav-link" href="#" id="sidebarToggle">		
				      <i class="fas fa-chevron-left"></i>
                <span class='menuText'>&nbsp;Collapse Menu</span>
          </a>
        </li>
		
		
        <li class="nav-item">
          <a class="nav-link" href="index.php">
            <img src='img/home.png' width='24px' >
            <span class='menuText'>Dashboard</span>
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="topics.php">
            <!-- <img src='img/internet.png' width='25px'> -->
            <i class="fas fa-star"></i>
            <span class='menuText'>&nbsp;Hot Topics</span></a>
        </li>
		
        <li class="nav-item">
          <a class="nav-link" href="internet.php">
            <img src='img/internet.png' width='25px'>
            <span class='menuText'>Internet</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">
            <img src='img/tv.png' width='25px'>
            <span class='menuText'>Digital TV</span></a>
        </li>
		<li class="nav-item">
          <a class="nav-link" href="#">
            <img src='img/homeMonitoring.png' width='25px'>
            <span class='menuText'>Home Monitoring</span></a>
        </li>
		<li class="nav-item">
          <a class="nav-link" href="#">
            <img src='img/homePhone.png' width='25px'>
            <span class='menuText'>Home Phone</span></a>
        </li>
		<li class="nav-item">
          <a class="nav-link" href="#">
            <img src='img/tv.png' width='25px'>
            <span class='menuText'>Ignite TV</span></a>
        </li>
		<li class="nav-item">
          <a class="nav-link" href="#">
            <img src='img/wireless.png' width='25px'>
            <span class='menuText'>Wireless</span></a>
        </li>
      </ul>