<?php
include 'header.php';
?>
      <div id="content-wrapper">

        <div class="container-fluid">

		  <h2>Dashboard</h2>
		  
			<br>

          <!-- Icon Cards-->
          <div class="row">
            <div class="col-xl-4 col-sm-6 mb-4">
			
              <div class="rogers-card">
		
				<h3>Internet</h3>
				<a href='#' class='card-link'>Packages <i class="fas fa-angle-right"></i></a><br>				
				<a href='#' class='card-link'>Self-Installation, Modem & Wi-Fi Setup <i class="fas fa-angle-right"></i></a><br>				
				<a href='#' class='card-link'>Installations & Technician Appointments <i class="fas fa-angle-right"></i></a><br>				
				<a href='#' class='card-link'>Change Package <i class="fas fa-angle-right"></i></a><br>				
        <a href='#' class='card-link'>Activate an Account <i class="fas fa-angle-right"></i></a><br>		
        <br>
              </div>
            </div>
			

            <div class="col-xl-8 col-sm-6 mb-8">
				<div class="rogers-card">
				<h3>Wireless</h3>

        <table width='100%'><tr><td>
				<a href='#' class='card-link'>2018 iPhone <i class="fas fa-angle-right"></i></a><br>				
				<a href='#' class='card-link'>Mobile Phone Plans <i class="fas fa-angle-right"></i></a><br>				
				<a href='#' class='card-link'>Mobile Phone Offers <i class="fas fa-angle-right"></i></a><br>				
				<a href='#' class='card-link'>Devices <i class="fas fa-angle-right"></i></a><br>				
        <a href='#' class='card-link'>Travel Outside Canada <i class="fas fa-angle-right"></i></a><br>  
        <a href='#' class='card-link'>Activate Account or Add-a-Line <i class="fas fa-angle-right"></i></a><br>  
        </td>

          <td>
          <a href='pdp.php' class='card-link'>Premium Device Protection <i class="fas fa-angle-right"></i></a><br>     
          <a href='#' class='card-link'>Billing & Online Billing <i class="fas fa-angle-right"></i></a><br>				
          <a href='#' class='card-link'>Change Plan <i class="fas fa-angle-right"></i></a><br>				
          <a href='#' class='card-link'>Data Top-ups <i class="fas fa-angle-right"></i></a><br>				
          <a href='#' class='card-link'>Data Overage Concerns <i class="fas fa-angle-right"></i></a><br>				
          <a href='#' class='card-link'>Payments, Refunds & Arrangements <i class="fas fa-angle-right"></i></a><br>	
        </td>

        </tr></table>  
              </div>
            </div>
			
      
        </div>
		
		
		<div class='row'>
		  <div class="col-xl-4 col-sm-6 mb-4">
        <div class="rogers-card">
        <h3>Digital TV</h3>
        
          <a href='#' class='card-link'>Channel Lineups <i class="fas fa-angle-right"></i></a><br>        
          <a href='#' class='card-link'>Packages & Value Added Services <i class="fas fa-angle-right"></i></a><br>       
          <a href='#' class='card-link'>Offers <i class="fas fa-angle-right"></i></a><br>        
          <a href='#' class='card-link'>Activate or Migrate an Account <i class="fas fa-angle-right"></i></a><br>       
          <a href='#' class='card-link'>Installation & Technician Appointments <i class="fas fa-angle-right"></i></a><br>  
          <a href='#' class='card-link'>Billing & Online Billing <i class="fas fa-angle-right"></i></a><br>  
              </div>
            </div>

		  <div class="col-xl-4 col-sm-6 mb-4">
				<div class="rogers-card">
				<h3>Home Monitoring</h3>
        
          <a href='#' class='card-link'>Packages <i class="fas fa-angle-right"></i></a><br>				
          <a href='#' class='card-link'>Prices & Specs <i class="fas fa-angle-right"></i></a><br>				
          <a href='#' class='card-link'>Move Requests <i class="fas fa-angle-right"></i></a><br>				
          <a href='#' class='card-link'>Troubleshoot Equipment <i class="fas fa-angle-right"></i></a><br>				
          <a href='#' class='card-link'>Change Account Info <i class="fas fa-angle-right"></i></a><br>	
          <br>

              </div>
            </div>
       
		  
		  
		    <div class="col-xl-4 col-sm-6 mb-4">
				<div class="rogers-card">
				<h3>Home Phone</h3>
				<a href='#' class='card-link'>Plans <i class="fas fa-angle-right"></i></a><br>				
				<a href='#' class='card-link'>Add-Ons <i class="fas fa-angle-right"></i></a><br>				
				<a href='#' class='card-link'>Change Account Info <i class="fas fa-angle-right"></i></a><br>				
				<a href='#' class='card-link'>Returns & Exchanges <i class="fas fa-angle-right"></i></a><br>				
        <a href='#' class='card-link'>Cancel, Move or Suspend Service <i class="fas fa-angle-right"></i></a><br>	
        <br>
              </div>
            </div>    
		
		</div>


       <div class="row">
            <div class="col-xl-4 col-sm-6 mb-4">
      
              <div class="rogers-card">
    
        <h3>Ignite TV</h3>
				<a href='#' class='card-link'>Channel Lineups & Channel Exchange <i class="fas fa-angle-right"></i></a><br>				
				<a href='#' class='card-link'>Packages <i class="fas fa-angle-right"></i></a><br>				
				<a href='#' class='card-link'>Features & Apps <i class="fas fa-angle-right"></i></a><br>				
				<a href='#' class='card-link'>Activate or Migrate an Account <i class="fas fa-angle-right"></i></a><br>				
				<a href='#' class='card-link'>Installation & Technician Appointments <i class="fas fa-angle-right"></i></i></a><br>	
        <a href='#' class='card-link'>Add or Remove Content <i class="fas fa-angle-right"></i></i></a><br> 
              </div>
            </div>
          </div>
          <br>
        <nav>
      <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Top News</a>
        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">My Popular Articles</a>
        <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Trending Articles</a>
      </div>
    </nav>
    <div class="tab-content" id="nav-tabContent">
      <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
        <a href='#' class='card-link'>Removal of Billing on Behalf (BOBO) and Employee Discount for Spotify/Texture <i class="fas fa-angle-right"></i></a><br>
        <a href='#' class='card-link'>Wrapped in Red - Black Friday Offers <i class="fas fa-angle-right"></i></a><br>       
        <a href='#' class='card-link'>Removal of Goodwill Adjustment Codes in SGI <i class="fas fa-angle-right"></i></a><br>       
        <a href='#' class='card-link'>Santa Tracker is Available for Ignite TV Customers <i class="fas fa-angle-right"></i></a>
      
      <hr style="border-top: dotted 2px #B1B4B8; background-color: #fff" /> 

        <a href='#' class='card-link'>View all Top News <i class="fas fa-angle-right"></i></a><br>
      </div>

      <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
      <a href='#' class='card-link'>Unlock an Apple iPhone <i class="fas fa-angle-right"></i></a><br>
        <a href='#' class='card-link'>Use the DeviceAid Feature in MyRogers App <i class="fas fa-angle-right"></i></a><br>       
        <a href='#' class='card-link'>Turn on Personal Hotspot on Apple iOS Devices <i class="fas fa-angle-right"></i></a><br>       
        <a href='#' class='card-link'>Wireless Technical Support (WTS) Tier 1 <i class="fas fa-angle-right"></i></a><br>
        <a href='#' class='card-link'>Wireless Monthly Plans <i class="fas fa-angle-right"></i></a>

      </div>
      
      <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
        <a href='#' class='card-link'>TV Channel Lookup for Grandfathered Packages and Bulk / Small Business Accounts <i class="fas fa-angle-right"></i></a><br>
        <a href='#' class='card-link'>Wireless Monthly Plans <i class="fas fa-angle-right"></i></a><br>       
        <a href='#' class='card-link'>TV Channel Lookup for Start, Select, Popular and Premier <i class="fas fa-angle-right"></i></a><br>       
        <a href='#' class='card-link'>Wireless Device Pricing <i class="fas fa-angle-right"></i></a><br>
        <a href='#' class='card-link'>Expried - Talk, Text and Wireless Internet Share Everything Price Plan (Unlimited Canada-Wide Calling, 500MB - 30GB) for $60 - $240 per Month <i class="fas fa-angle-right"></i></a>

      </div>
    </div>




		
		</div></div>
        <!-- /.container-fluid -->

      <?php
include 'footer.php';
?>